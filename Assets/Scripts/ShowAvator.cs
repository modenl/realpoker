﻿using UnityEngine;
using System.Collections;

public class ShowAvator : MonoBehaviour {

    private string url = @"http://i.imgur.com/tFF9l.png";
    IEnumerator Start() {
        WWW www = new WWW(url);
        yield return www;
        var texture = gameObject.GetComponent<GUITexture>();
        renderer.material.mainTexture = www.texture;
        renderer.material.shader = Shader.Find("Unlit/Transparent");

    }

	// Update is called once per frame
	void Update () {
	
	}
}
