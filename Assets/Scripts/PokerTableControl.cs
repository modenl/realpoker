﻿using UnityEngine;
using System.Collections;
using System;

public class PokerTableControl : MonoBehaviour
{
    public int PlayerCount = 2;
    public PokerPlayerControl[] players;
    public GameObject PlayerPrefab;
    public GameObject MoneyLabels;
    public CardDeck Deck;
    const float FlyTime = 0.5f;
    int dealerPos = -1;
    float sb = 10.0f;
    UILabel[] labels;

    int betterPos;

    void Buyin(int position, float buyin)
    {
        players [position] = Instantiate(PlayerPrefab) as PokerPlayerControl;
        players [position].Buyin(buyin);
    }

    IEnumerator SimulateNetwork()
    {
        OnGameSetup(10, 2);
        yield return new WaitForSeconds(0.5f);
        OnSelectDealer(0);
        yield return new WaitForSeconds(0.5f);
        OnPostSB(0);

        yield return new WaitForSeconds(0.5f);
        OnPostBB(1);
        yield return new WaitForSeconds(0.5f);
        OnDealPreflop();
        yield return new WaitForSeconds(0.5f);
        OnDealFlop();
        yield return new WaitForSeconds(0.5f);
        OnDealTurn();
        yield return new WaitForSeconds(0.5f);
        OnDealRiver();
    }

    // Use this for initialization
    void Start()
    {
        Deck.Initialize();
        Deck.Reset();
        Deck.Shuffle();
        players = new PokerPlayerControl[PlayerCount];
        for (int i=0; i<PlayerCount; i++)
        {
            players [i] = Instantiate(PlayerPrefab) as PokerPlayerControl;
            //players[i].Buyin(1000);
        }
        this.labels = MoneyLabels.GetComponentsInChildren<UILabel>();
        Array.Sort(this.labels, delegate(UILabel x,UILabel y)
        {
            return x.name.CompareTo(y.name);
        });

        StartCoroutine(SimulateNetwork());
    }

    Vector3 GetDeckPosition()
    {
        return new UnityEngine.Vector3(0, 3, 0);
    }

    void DealCard(int pos, int number)
    {
        CardDef c1 = Deck.Pop();
        Debug.Log(c1);
        if (c1 != null)
        {
            GameObject newObj = new GameObject();
            newObj.name = "Card";
            Card newCard = newObj.AddComponent(typeof(Card)) as Card;
            newCard.Definition = c1;
            newObj.transform.parent = Deck.transform;
            newObj.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);

            newCard.TryBuild();
            float x = 8.1f * (pos * 2 - 1) -0.1f + number*0.3f;
            float y = 1f - number * 0.1f;
            float z = -0.01f*number;
            Vector3 deckPos = GetDeckPosition();
            newObj.transform.position = deckPos;
            //m_dealer.Add(newCard);
            Debug.Log(deckPos);
            newCard.SetFlyTarget(deckPos, new Vector3(x, y, z), FlyTime);
        }
    }

    void DealCardOnBoard(int number)
    {
        CardDef c1 = Deck.Pop();
        Debug.Log(c1);
        if (c1 != null)
        {
            GameObject newObj = new GameObject();
            newObj.name = "Card";
            Card newCard = newObj.AddComponent(typeof(Card)) as Card;
            newCard.Definition = c1;
            newObj.transform.parent = Deck.transform;
            newObj.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
            newCard.TryBuild();
            float x = -3f + number*1.6f;
            float y = 1f;
            float z = -0.01f*number;
            Vector3 deckPos = GetDeckPosition();
            newObj.transform.position = deckPos;
            newCard.SetFlyTarget(deckPos, new Vector3(x, y, z), FlyTime);
        }
    }

    [RPC]
    void OnGameSetup(float sb, int playerCount)
    {
        this.sb = sb;
        this.PlayerCount = playerCount;
    }

    [RPC]
    void OnSelectDealer(int dealerPos)
    {
        // Show dealer button at dealer position
        this.dealerPos = dealerPos;
    }

    [RPC]
    void OnSelectSB()
    {

    }

    [RPC]
    void OnPostSB(int sbPos)
    {
        labels [sbPos].text = "" + sb;

    }

    [RPC]
    void OnSelectBB()
    {

    }

    [RPC]
    void OnPostBB(int bbPos)
    {
        labels [bbPos].text = "" + (2 * sb);
    }

    [RPC]
    void OnDealPreflop()
    {
        DealCard(0, 0);
        DealCard(0, 1);
        DealCard(1, 0);
        DealCard(1, 1);
    }

    [RPC]
    void OnSetBetter(int betterPos) {
        this.betterPos = betterPos;
    }

    [RPC]
    void OnDealFlop() {
        DealCardOnBoard(0);
        DealCardOnBoard(1);
        DealCardOnBoard(2);
    }

    [RPC]
    void OnDealTurn()
    {
        DealCardOnBoard(3);
    }

    [RPC]
    void OnDealRiver()
    {
        DealCardOnBoard(4);
    }

    int FindNextActivePlayerFrom(int current)
    {
        for (int i=current + 1; i < current + PlayerCount; i++)
        {
            int pos = i % PlayerCount;
            PokerPlayerControl p = players [pos];
            if (p.IsPlaying())
            {
                return pos;
            }
        }
        return -1;
    }
    // Update is called once per frame
    void Update()
    {

    }
//
//    void HighLight(GameObject go) {
//        Renderer renderer = go.GetComponentInChildren<Renderer>() as Renderer;
//        Shader highlightedShader = Shader.Find("Toon/Lighted Outline"); // Replace with whatever shader you want
//        renderer.material.shader = highlightedShader;
//    }
}
