﻿using UnityEngine;
using System.Collections;

public class Hud : MonoBehaviour {
    public GameObject target;
    private Camera mainCamera;
    private Camera guiCamera;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        mainCamera = NGUITools.FindCameraForLayer(target.layer);
        guiCamera = NGUITools.FindCameraForLayer(gameObject.layer);
        var vp = mainCamera.WorldToViewportPoint(target.transform.position);
        var guip = guiCamera.ViewportToWorldPoint(vp);
        transform.position = guip;
	}
}
