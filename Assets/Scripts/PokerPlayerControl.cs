﻿using UnityEngine;
using System.Collections;

public class PokerPlayerControl : MonoBehaviour {
   
    float buyin;

    PlayMakerFSM Fsm()
    {
        return GetComponent<PlayMakerFSM>();
    }

    public bool IsPlaying() {
        return Fsm().ActiveStateName == "SitIn";
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Buyin(float buyin)
    {
        this.buyin = buyin;
    }
}
